<?php

require_once 'lib/Singleton.php';

/*
Plugin Name: Apple Mod
Description: Plugin wspomagający udostępnianie usług naprawiania urządzeń Apple.
Version: 1.0.0
Author: Rafał Głuszak
Author URI: http://www.reedy.pl
License: GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: apple-mod
Domain Path: /langs

	Apple Mod is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	any later version.

	Apple Mod is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License along
	with Apple Mod. If not, see https://www.gnu.org/licenses/gpl-2.0.html.
*/

if (!class_exists('Apple')) {

    /**
     * Class Apple
     */
    final class Apple extends Singleton
    {

        /**
         *
         */
        public function install()
        {
            add_option('apple_options', []);
            add_option('apple_orders', []);
        }

        /**
         *
         */
        public function uninstall()
        {
            delete_option('apple_options');
            delete_option('apple_orders');
        }

        /**
         * @return $this
         */
        public function post_type()
        {

            $labels = [
                'name' => 'Oferta',
                'singular_name' => 'Oferta',
                'menu_name' => 'Oferty',
                'name_admin_bar' => 'Ofertę',
                'add_new' => 'Nowa oferta'
            ];

            $args = [
                'labels' => $labels,
                'public' => true,
                'capability_type' => 'page',
                'has_archive' => true,
                'hierarchical' => true,
                'supports' => [
                    'title',
                    'editor',
                    'author',
                    'thumbnail',
                    'excerpt',
                    'comments',
                    'page-attributes',
                    'custom-fields',
                    'post-formats'
                ]
            ];

            register_post_type('offer', $args);
        }

        /**
         *
         */
        public function overview()
        {
            if (isset($_GET['action']) && $_GET['action'] === 'more') {
                require_once PLUGIN_ADMINISTRATOR_DIR . 'details.php';
                exit();
            }

            require_once PLUGIN_ADMINISTRATOR_DIR . 'overview.php';
        }

        /**
         *
         */
        public function register_settings() {
            register_setting('apple_mod_options', 'email');
            register_setting('apple_mod_options', 'currency');
        }


        /**
         *
         */
        public function settings()
        {
            require_once PLUGIN_ADMINISTRATOR_DIR . 'options.php';
        }

        /**
         * @return $this
         */
        public function menu()
        {
            add_menu_page('Apple Zamówienia', 'Apple Zamówienia', 'manage_options', 'apple', function () {
                $this->overview();
            });

            add_options_page('Apple Zamówienia', 'Apple Zamówienia', 'manage_options', 'apple_settings', function () {
                $this->settings();
            });

            return $this;
        }

        /**
         *
         */
        public function paths()
        {
            define('PLUGIN_ASSETS_DIR', plugin_dir_url(__FILE__) . 'assets/');
            define('PLUGIN_ADMINISTRATOR_DIR', plugin_dir_path(__FILE__) . 'administrator/');
            define('PLUGIN_CLASSES_DIR', plugin_dir_path(__FILE__) . 'classes/');
            define('PLUGIN_VIEWS_DIR', plugin_dir_path(__FILE__) . 'views/');
            define('PLUGIN_INC_DIR', plugin_dir_path(__FILE__) . 'inc/');
        }

        /**
         *
         */
        public function enqueue_admin_scripts()
        {

        }

        /**
         *
         */
        public function enqueue_scripts()
        {
            /* Basket scripts */

            if (class_exists('Basket')) {

                wp_register_style('basket', PLUGIN_ASSETS_DIR . 'css/basket.min.css');
                wp_enqueue_style('basket');

                wp_enqueue_script('basket', PLUGIN_ASSETS_DIR . 'js/basket.min.js', [], '1.0.0', true);
            }
        }

        /**
         * @return $this
         */
        public function actions()
        {
            register_activation_hook(__FILE__, function () {
                $this->install();
                $this->register_settings();
            });

            register_deactivation_hook(__FILE__, function () {
                $this->uninstall();
            });

            add_action('init', function () {
                ob_start();

                $this->post_type();
                $this->register_settings();
            });

            add_action('admin_init', function () {
                ob_start();

                $this->post_type();
                $this->register_settings();
            });

            add_action('plugins_loaded', function () {
                $this->paths();
            }, 20);

            if (is_admin()) {
                add_action('admin_menu', function () {
                    $this->menu();
                });
                add_action('admin_enqueue_scripts', function () {
                    $this->enqueue_admin_scripts();
                }, 20);
            }

            add_action('wp_enqueue_scripts', function () {
                $this->enqueue_scripts();
            }, 20);

            add_action('orders_delete_success', function ($items) {
                echo sprintf('<div class="updated"><p>%s</p></div>', $items['message']);
            });

            add_action('order_update_success', function ($items) {
                echo sprintf('<div class="updated"><p>%s</p></div>', $items['message']);
            });

            return $this;
        }

        /**
         * @param $class
         * @return mixed
         */
        public function factory($class)
        {
            switch ($class) {
                case 'basket' :
                    return new Basket();
                break;
                case 'validation' :
                    return new Validation();
                break;
                case 'order' :
                    return new Order();
                break;
                case 'mailer' :
                    return new Mailer();
                break;
                default :
                    return false;
            }
        }

        /**
         *
         */
        public function init()
        {
            $this->actions();
        }
    }

    /** @var Apple $apple */
    $apple = Apple::getInstance();
    $apple->init();
}
