<?php

    /** @var Apple $apple */
    $apple = Apple::getInstance();

    /** @var Order $orders */
    $orders = $apple->factory('order');

    $orderData = $orders->getOrders($_GET['order']);

    $currency = get_option('currency');

    if (isset($_POST['save-status'])) {
        if ($orders->updateOrder($_GET['order'], 'status', $_POST['status'])) {

            /** @var Mailer $mailer */
            $mailer = $apple->factory('mailer');

            $mailContent = $mailer->setMailsPath(PLUGIN_INC_DIR . 'mails.php')
                ->getMailContent('status');

            $variablesToMail = [
                '{oid}',
                '{oldstatus}',
                '{newstatus}'
            ];

            $fields = [
                $_GET['order'],
                Types\OrderStatusTypes::getChoices()[$orderData['status']],
                Types\OrderStatusTypes::getChoices()[$_POST['status']]
            ];

            $mailContent['content'] = str_replace($variablesToMail, $fields, $mailContent['content']);

            $mailer->setEmailData([
                'reciver' => get_option('email'),
                'requester' => $orderData['customer']['email'],
                'subject' => $mailContent['subject'],
                'content' => $mailContent['content']
            ])
                ->setHTML(true)
                ->send();
        }

        $_POST['_wp_http_referer'] = add_query_arg('updated', 1, $_POST['_wp_http_referer']);
        wp_redirect($_POST['_wp_http_referer']);
        exit();
    }

?>

<div class="wrap">
    <h1>Szczegóły zamówienia nr <?php echo $_GET['order']; ?> (<?php echo $orderData['customer']['created']; ?>)</h1>

    <?php
        if (isset($_GET['updated'])) {
            do_action('order_update_success', [
                'message' => 'Status zamówienia został zaaktualizowany!'
            ]);
        }
    ?>
    <h4>Status zamówienia: <?php echo Types\OrderStatusTypes::getChoices()[$orderData['status']]; ?></h4>
    <section>
        <h3>Dane kupującego</h3>

        <p>
            <strong>Imię i nazwisko:</strong>
            <br><?php echo $orderData['customer']['name'] . ' ' . $orderData['customer']['surname']; ?>
            <br><?php echo $orderData['customer']['email']; ?>
        </p>

        <p>
            <strong>Adres:</strong>
            <?php

                $local = '';

                if ($orderData['customer']['local'] !== 0) {
                    $local = '/' . $orderData['customer']['local'];
                }
            ?>
            <br>
            <?php echo $orderData['customer']['street'] . ' ' . $orderData['customer']['number'] . $orderData['customer']['local']; ?>
            <br>
            <?php echo $orderData['customer']['zip'] . ' ' . $orderData['customer']['city']; ?>
        </p>
    </section>
    <section>
        <h3>Kupione usługi</h3>
        <?php $summaryPrice = 0; ?>
        <table class="wp-list-table widefat fixed striped">
            <tr>
                <th>Usługa</th>
                <th>Cena</th>
            </tr>
            <?php foreach ($orderData['products'] as $product) { ?>
                <tr>
                    <td><?php echo $product['title']; ?></td>
                    <td><?php echo $product['price'] . ' ' . $currency; ?></td>
                </tr>
            <?php
                $summaryPrice += $product['price'];
            } ?>
        </table>
        <p>Do zapłaty<strong>: <?php echo $summaryPrice . ' ' . $currency . '</strong>'; ?></p>
    </section>
    <section>
        <form method="post" action="">
            <table>
                <tr>
                    <td><label>Ustaw status zamówienia:</label></td>
                    <td>
                        <select name="status">
                            <?php foreach(Types\OrderStatusTypes::getChoices() as $key => $value) {
                                $selected = '';
                                if ($orderData['status'] === $key) {
                                    $selected = 'selected="selected"';
                                }
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <input type="hidden" name="_wp_http_referer" value="<?php echo $_SERVER['REQUEST_URI']; ?>">
                    <td colspan="2"><input type="submit" name="save-status" class="button button-primary" value="Zatwierdź zmiany"></td>
                </tr>
            </table>
        </form>
    </section>
</div>