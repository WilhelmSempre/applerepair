<div class="wrap">
    <h2>Ustawienia Apple Mod</h2>
    <form method="post" action="options.php">
    <table class="form-table">
        <tr>
            <th scope="row"><label>Waluta:</label></th>
            <td scope="row">
                <select name="currency">
                    <option <?php echo esc_attr(get_option('currency')) === 'PLN' ? 'selected="selected"' : ''; ?> value="PLN">PLN</option>
                    <option <?php echo esc_attr(get_option('currency')) === '€' ? 'selected="selected"' : ''; ?> value="€">EUR</option>
                    <option <?php echo esc_attr(get_option('currency')) === '$' ? 'selected="selected"' : ''; ?>  value="$">USD</option>
                </select>
            </td>
        </tr>
        <tr>
            <th scope="row"><label>Adres e-mail do powiadomień o zamówieniach:</label></th>
            <td scope="row">
                <input type="email" name="email" value="<?php echo esc_attr(get_option('email')); ?>">
            </td>
        </tr>
    </table>
    <?php settings_fields('apple_mod_options'); ?>
    <input type="hidden" name="action" value="update">
        <?php submit_button(); ?>
    </form>
</div>
