<div class="wrap">
    <h1>Zamówienia</h1>

    <?php
        require_once PLUGIN_INC_DIR . 'table.php';
        $table = new AppleTable();

        /** @var Apple $apple */
        $apple = Apple::getInstance();

        /** @var Order $orders */
        $orders = $apple->factory('order');

        $orderData = $orders->getOrders();

        $ordersToTable = [];

        foreach ($orderData as $key => $order) {
            $ordersToTable[] = [
                'ID' => $key,
                'oid' => $key,
                'mail' => $order['customer']['email'],
                'status' => Types\OrderStatusTypes::getChoices()[$order['status']],
                'created' => $order['customer']['created']
            ];
        }
    ?>
    <form id="orders" action="<?php echo get_the_permalink(); ?>" method="post">
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>">
        <?php $table->render($ordersToTable); ?>
    </form>
</div>
