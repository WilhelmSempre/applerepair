'use strict';

var basket = basket || {};

basket = {
    actionSelectBox : function(actionSelect) {

        $(actionSelect).on('click', function(event) {
            var that = $(this);

            if (that.hasClass('all')) {
                var checkboxes = $(actionSelect).not('.all');

                if (!that.is(':checked')) {
                    checkboxes.prop('checked', false);
                } else {
                    checkboxes.prop('checked', true);
                }
            }
        });

        return basket;
    },

    init : function() {
        basket.actionSelectBox('input.action');
    }
};

$(document).ready(function() {
    basket.init();
});