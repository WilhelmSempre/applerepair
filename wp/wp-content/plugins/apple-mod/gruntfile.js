'use strict';

module.exports = function(grunt) {
    require('grunt-timer').init(grunt);

    require('load-grunt-tasks')(grunt, {
        pattern: ['grunt-*', '!grunt-timer']
    });

  
  grunt.initConfig({
      svgstore: {
        options: {
          prefix : 'shape-',
        },
        default : {
          files: {
            'assets/img/icons/svg-defs.svg': ['assets/img/icons/*.svg'],
          }
        }
      },
      cssmin: {
          css: {
              files: {
                  'assets/css/basket.min.css' : 'assets/css/basket.css'
              }
          }
      },
      uglify: {
        options: {
          mangle: false
        },
        my_target: {
          files: {
            'assets/js/basket.min.js': ['assets/js/basket.js']
          }
        }
      },
      watch: {
          sass: {
              files: ['*.html', 'assets/css/*.css','assets/js/*.js'],
              tasks: ['cssmin','uglify']
          }
      }
    });

    grunt.loadNpmTasks('grunt-svgstore');

    grunt.registerTask('default', ['cssmin', 'svgstore', 'uglify', 'watch']);
};