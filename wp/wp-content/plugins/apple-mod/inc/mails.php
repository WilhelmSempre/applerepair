<?php

$mails = [
    'order' => [
        'subject' => 'Złożenie zamówienia w serwisie',
        'content' => <<<'MAIL'
<p>Zostało złożone zamówienie w serwisie!</p>
<p><strong>Dane zamówienia: </strong></p>
<p>{name} {surname}</p>
<p>{street} {number}</p>
<p>{zip} {city}</p>
<p><strong>Numer zamówienia: </strong> {oid}</p>
<p>Przejdź do panelu administracyjnego aby zweryfikować zamówienie!</p>
MAIL
    ],

    'status' => [
        'subject' => 'Zmiana statusu zamówienia',
        'content' => <<<'MAIL'
<p>Status twojego zamówienia o numerze {oid} uległ zmianie z <strong>{oldstatus}</strong> na <strong>{newstatus}</strong>.</p>
<p>Pozdrawiamy!</p>
MAIL
    ]
];