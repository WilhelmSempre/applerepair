<?php


if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

/**
 * Class AppleTable
 */
class AppleTable extends WP_List_Table
{
    /**
     * @param $data
     */
    public function prepare($data)
    {
        $columns = $this->get_columns();
        $hidden = $this->get_hidden_columns();
        $sortable = $this->get_sortable_columns();
        $data = $this->table_data($data);

        $this->process_bulk_action();

        usort($data, function ($a, $b) {
            return $this->sort_data($a, $b);
        });

        $perPage = 10;

        $currentPage = $this->get_pagenum();
        $totalItems = count($data);

        $this->set_pagination_args([
            'total_items' => $totalItems,
            'per_page' => $perPage
        ]);

        $data = array_slice($data, (($currentPage - 1) * $perPage), $perPage);
        $this->_column_headers = [$columns, $hidden, $sortable];
        $this->items = $data;
    }

    /**
     * @return array
     */
    public function get_columns()
    {
        $columns = [
            'cb' => '<input type="checkbox">',
            'oid' => 'Numer zamówienia',
            'mail' => 'Email',
            'status' => 'Status zamówienia',
            'created' => 'Data utworzenia'
        ];

        return $columns;
    }

    /**
     * @return array
     */
    public function get_bulk_actions() {
        $actions = [
            'delete' => 'Usuń'
        ];

        return $actions;
    }

    /**
     *
     */
    public function process_bulk_action() {

        if (isset($_GET['deleted'])) {
            do_action('orders_delete_success', [
                'message' => 'Wybrane zamówienia zostały usunięte!'
            ]);
        }

        if('delete' === $this->current_action()) {

            /** @var Apple $apple */
            $apple = Apple::getInstance();

            /** @var Order $orders */
            $orders = $apple->factory('order');

            $orderIDs = $_POST['order'];

            foreach ($orderIDs as $orderId) {
                $orders->removeOrder($orderId);
            }

            $_POST['_wp_http_referer'] = add_query_arg('deleted', 1, $_POST['_wp_http_referer']);
            wp_redirect($_POST['_wp_http_referer']);
            exit();
        }
    }

    /**
     * @return array
     */
    public function get_hidden_columns()
    {
        return [];
    }

    /**
     * @return array
     */
    public function get_sortable_columns()
    {
        return [
            'offer' => ['offer', false],
            'oid' => ['oid', false],
            'status' => ['status', false],
            'created' => ['created', false]
        ];
    }

    /**
     * @param $data
     * @return array
     */
    private function table_data($data)
    {
        return $data;
    }

    /**
     * @param $item
     * @return string
     */
    function column_oid($item){
        $actions = [
            'more' => sprintf('<a href="?page=%s&action=%s&order=%s">Szczegóły</a>',$_REQUEST['page'],'more',$item['ID']),
        ];

        return sprintf('%1$s%3$s',
            $item['oid'],
            $item['ID'],
            $this->row_actions($actions)
        );
    }

    /**
     * @param object $item
     * @return string
     */
    function column_cb($item){
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            'order',
            $item['ID']
        );
    }

    /**
     * @param object $item
     * @param string $column_name
     * @return mixed
     */
    public function column_default($item, $column_name)
    {
        return $item[$column_name];
    }

    /**
     * @param $a
     * @param $b
     * @return int
     */
    private function sort_data($a, $b)
    {

        $orderby = 'oid';
        $order = 'asc';

        if (!empty($_GET['orderby'])) {
            $orderby = $_GET['orderby'];
        }

        if (!empty($_GET['order'])) {
            $order = $_GET['order'];
        }
        $result = strcmp($a[$orderby], $b[$orderby]);
        if ($order === 'asc') {
            return $result;
        }
        return -$result;
    }

    /**
     * @param $data
     */
    public function render($data = [])
    {
        $this->prepare($data);

        parent::display();
    }

}
