<?php

/**
 * Class Basket
 */
final class Basket
{

    /**
     * Basket constructor.
     */
    public function __construct()
    {
        $this->initShortcodes()
            ->actions();
    }

    /**
     * @return $this
     */
    public function actions()
    {
        add_action('init', function () {
            ob_start();
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function deleteBasket()
    {
        setcookie('apple-mod-basket', '', -1, COOKIEPATH, COOKIE_DOMAIN);
        return $this;
    }

    /**
     * @param array $product
     * @param int $time
     * @return $this
     */
    public function createBasket($product = [], $time = 60 * 60 * 60 * 24)
    {
        setcookie('apple-mod-basket', json_encode($product), time() + $time, COOKIEPATH, COOKIE_DOMAIN);
        return $this;
    }

    /**
     * @return mixed
     */
    public function readBasket()
    {
        if (isset($_COOKIE['apple-mod-basket'])) {
            return json_decode(stripslashes($_COOKIE['apple-mod-basket']), true);
        }

        return [];
    }

    /**
     * @param array $product
     * @return $this
     */
    public function addProduct($product = [])
    {

        $productsList = $this->readBasket();

        $this->deleteBasket();

        $productsList[] = $product;

        $this->createBasket($productsList);

        return $this;
    }

    /**
     * @param $title
     * @return $this
     */
    public function removeProduct($title)
    {

        $productsList = $this->readBasket();

        $this->deleteBasket();

        foreach ($productsList as $key => $product) {
            if ($product['title'] === $title) {
                unset($productsList[$key]);
            }
        }

        $this->createBasket($productsList);

        return $this;
    }

    /**
     * @param int|null $productId
     * @return int
     */
    public function getProductsCount($productId = null)
    {
        $productsList = $this->readBasket();

        if ($productId) {
            $filteredProducts = [];

            foreach ($productsList as $product) {
                if ($product['id'] === $productId) {
                    $filteredProducts[] = $product;
                }
            }

            return count($filteredProducts);
        }

        return count($productsList);
    }

    /**
     * @return $this
     */
    public function initShortcodes()
    {
        add_shortcode('product-list', function () {
            if (file_exists(get_template_directory() . '/apple-mod/_products-list.php')) {
                require_once get_template_directory() . '/apple-mod/_products-list.php';
            } else {
                require_once PLUGIN_VIEWS_DIR . '/_products-list.php';
            };
        });

        add_shortcode('product-summary', function () {
            if (file_exists(get_template_directory() . '/apple-mod/_products-summary.php')) {
                require_once get_template_directory() . '/apple-mod/_products-summary.php';
            } else {
                require_once PLUGIN_VIEWS_DIR . '/_products-summary.php';
            };
        });

        add_shortcode('product-finish', function () {
            if (file_exists(get_template_directory() . '/apple-mod/_products-finish.php')) {
                require_once get_template_directory() . '/apple-mod/_products-finish.php';
            } else {
                require_once PLUGIN_VIEWS_DIR . '/_products-finish.php';
            };
        });

        return $this;
    }

    /**
     *
     */
    public function productsListAction()
    {
        if (isset($_POST['do-action'])) {
            $productsIds = $_POST['ids'];

            if ($_POST['selected-action-list'] === 'delete') {
                foreach ($productsIds as $id) {
                    $this->removeProduct(get_the_title($id));
                }
            }

            wp_redirect(get_the_permalink(), 302);
        }
    }
}