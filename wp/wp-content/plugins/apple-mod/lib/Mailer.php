<?php

/**
 * Class Mailer
 */
final class Mailer
{

    /**
     * @var array
     */
    private $emailData = [];

    /**
     * @var bool
     */
    private $html = false;

    /**
     * @var
     */
    private $mailsPath = PLUGIN_INC_DIR . 'mails.php';

    /**
     * @param array $email
     * @return $this
     */
    public function setEmailData($email = [])
    {
        $this->emailData = $email;
        return $this;
    }

    /**
     * @return array
     */
    public function getEmailData()
    {
        return $this->emailData;
    }

    /**
     * @param $html
     * @return $this
     */
    public function setHTML($html)
    {
        $this->html = $html;
        return $this;
    }

    /**
     * @param $path
     * @return $this
     */
    public function setMailsPath($path)
    {
        $this->mailsPath = $path;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMailsPath()
    {
        return $this->mailsPath;
    }

    /**
     * @param null $id
     * @return null|array
     */
    public function getMailContent($id = null)
    {
        $mails = null;

        require_once $this->mailsPath;

        if ($id) {
            return $mails[$id];
        }

        return $mails;
    }

    /**
     * @return bool
     */
    public function getHTML()
    {
        return $this->html;
    }

    /**
     * @throws Exception
     */
    public function send()
    {
        $headers = '';

        if (!array_key_exists('reciver', $this->emailData)) {
            throw new Exception('Reciver not found!');
        }

        if (!array_key_exists('requester', $this->emailData)) {
            throw new Exception('Requester not found!');
        }

        if (!array_key_exists('subject', $this->emailData)) {
            throw new Exception('Subject not found!');
        }

        if (!array_key_exists('content', $this->emailData)) {
            throw new Exception('Content not found!');
        }

        if (array_key_exists('headers', $this->emailData)) {
            $headers = $this->emailData['headers'];
        }

        if ($this->html) {
            $headers .= 'Content-type:text/html;charset=UTF-8' . "\r\n";
        }

        return wp_mail($this->emailData['reciver'],
            $this->emailData['subject'],
            $this->emailData['content'],
            $headers);
    }
}