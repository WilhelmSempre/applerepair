<?php

defined('ABSPATH') || die('No script kiddies please!');

/**
 * Class Order
 */
final class Order
{

    /**
     * @var array
     */
    public $customerData = [];

    /**
     * @var array
     */
    public $productsData = [];

    /**
     * @var
     */
    private $orderID;

    /**
     * @param array $customer
     * @return $this
     */
    public function setCustomer($customer = [])
    {
        $this->customerData = $customer;
        return $this;
    }

    /**
     * @param null|int $id
     * @return array
     */
    public function getCustomer($id = null)
    {
        if ($id) {
            return $this->customerData[$id];
        }

        return $this->customerData;
    }

    /**
     * @param array $products
     * @return $this
     */
    public function setProducts($products = [])
    {
        $this->productsData = $products;
        return $this;
    }

    public function getProducts($id = null)
    {
        if ($id) {
            return $this->productsData[$id];
        }

        return $this->productsData;
    }

    /**
     * @return bool
     */
    public function saveOrder()
    {
        $orders = get_option('apple_orders');

        $this->orderID = str_pad(rand(0, 999999999), 9, 0, STR_PAD_LEFT);

        $this->customerData['created'] = (new DateTime())->format('d-m-Y H:i:s');

        $orders[trim($this->orderID)] = [
            'customer' => $this->customerData,
            'products' => $this->productsData,
            'status' => Types\OrderStatusTypes::REGISTERED
        ];

        return update_option('apple_orders', $orders);
    }

    /**
     * @return bool
     */
    public function getLastOrderID()
    {
        if ($this->orderID) {
            return $this->orderID;
        }

        return false;
    }

    /**
     * @param $orderID
     * @return bool
     */
    public function removeOrder($orderID)
    {
        if (!$orderID) {
            return false;
        }

        $orders = get_option('apple_orders');

        foreach ($orders as $key => $order) {
            if ($key === (int) $orderID) {
                unset($orders[$orderID]);
            }
        }

        return update_option('apple_orders', $orders);
    }

    /**
     * @param $orderID
     * @return bool
     */
    public function updateOrder($orderID, $key, $value = null)
    {
        if (!$orderID || !$key) {
            return false;
        }

        $orders = get_option('apple_orders');

        if ($orders[$orderID][$key] === $value) {
            return false;
        }

        foreach ($orders[$orderID] as $index => $order) {
            if ($key === $index) {
                $orders[$orderID][$key] = (int) $value;
            }
        }

        return update_option('apple_orders', $orders);
    }

    /**
     * @param null $orderID
     * @return bool|mixed|void
     */
    public function getOrders($orderID = null)
    {
        if ($orderID) {
            return get_option('apple_orders')[$orderID];
        }

        return get_option('apple_orders');
    }
}