<?php

namespace Types;

/**
 * Class OrderStatusTypes
 */
class OrderStatusTypes extends \Singleton
{

    const REGISTERED = 0;
    const PAYED = 1;
    const REALIZATING = 2;
    const FINISHED = 3;


    /**
     * @return array
     */
    public static function getChoices()
    {
        return [
            self::REGISTERED => 'Zarejestrowany',
            self::PAYED => 'Zapłacony',
            self::REALIZATING => 'W trakcie realizacji',
            self::FINISHED => 'Sfinalizowany'
        ];
    }
}