<?php

/**
 * Class Validation
 */
final class Validation
{
    /**
     * @var array
     */
    private $errors = [];

    /**
     * @var array
     */
    private $criterium = [];

    /**
     * @var array
     */
    private $fields = [];

    /**
     * @param array $fields
     * @return $this
     */
    public function setFields($fields = [])
    {
        $this->fields = $fields;
        return $this;
    }

    /**
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @param array $criterium
     * @return $this
     */
    public function setCriterium($criterium = [])
    {
        $this->criterium = $criterium;
        return $this;
    }

    /**
     * @param array $errors
     * @return $this
     */
    public function setErrors($errors = [])
    {
        $this->errors = $errors;
        return $this;
    }

    /**
     * @param $key
     * @return array
     */
    public function getErrors($key = null)
    {
        if ($key) {
            return $this->errors[$key];
        }

        return $this->errors;
    }

    /**
     * @param $key
     * @return bool
     */
    public function isError($key = null) {
        if ($key) {
            return isset($this->errors[$key]);
        }

        return count($this->errors) > 0;
    }

    /**
     * @return array
     */
    private function createCriteriumFromArray()
    {
        $criteriumPieces = [];

        foreach ($this->criterium as $key => $criterium) {
            $criteriumPieces[$key] = explode('|', $criterium);
            foreach ($criteriumPieces[$key] as $index => $piece) {
                if (strpos($piece, ':') !== false) {
                    $criteriumPieces[$key][] = explode(':', $piece);
                    unset($criteriumPieces[$key][$index]);
                }
            }
        }

        return $criteriumPieces;
    }

    /**
     * @param $fieldValue
     * @return bool
     */
    private function checkRequired($fieldValue)
    {
        return $fieldValue !== '';
    }


    /**
     * @param $fieldValue
     * @return int|bool
     */
    private function checkAlpha($fieldValue)
    {
        if ($fieldValue !== '') {
            return preg_match('/^[\s\p{L}]+$/u', $fieldValue);
        }

        return true;
    }

    /**
     * @param $fieldValue
     * @return int|bool
     */
    private function checkAlphaNum($fieldValue)
    {
        if ($fieldValue !== '') {
            return preg_match('/^[\s\p{L}0-9]+$/u', $fieldValue);
        }

        return true;
    }

    /**
     * @param $fieldValue
     * @return int|bool
     */
    private function checkNum($fieldValue)
    {
        if ($fieldValue !== '') {
            return preg_match('/^[0-9]+$/u', $fieldValue);
        }

        return true;
    }

    /**
     * @param $fieldValue
     * @return mixed|bool
     */
    private function checkEmail($fieldValue)
    {
        if ($fieldValue !== '') {
            return filter_var($fieldValue, FILTER_VALIDATE_EMAIL);
        }

        return true;
    }

    /**
     * @param $fieldValue
     * @param $max
     * @return bool|bool
     */
    private function checkMax($fieldValue, $max)
    {
        if ($fieldValue !== '') {
            return strlen($fieldValue) < $max;
        }

        return true;
    }

    /**
     * @param $fieldValue
     * @param $min
     * @return bool|bool
     */
    private function checkMin($fieldValue, $min)
    {
        if ($fieldValue !== '') {
            return strlen($fieldValue) > $min;
        }

        return true;
    }

    /**
     * @param $fieldValue
     * @param $pattern
     * @return int|bool
     */
    private function checkMatch($fieldValue, $pattern)
    {
        if ($fieldValue !== '') {
            return preg_match($pattern, $fieldValue);
        }

        return true;
    }

    /**
     * @throws Exception
     */
    public function validate()
    {
        $criteriumKeys = array_keys($this->criterium);
        $fieldsKeys = array_keys($this->fields);

        if (count(array_diff($criteriumKeys, $fieldsKeys)) > 0) {
            throw new Exception('Criterium and fields values does not match!');
        }

        $fieldsWithCriterias = $this->createCriteriumFromArray();

        foreach ($fieldsWithCriterias as $key => $fields) {
            foreach ($fields as $criterias) {
                if (is_array($criterias)) {
                    switch ($criterias[0]) {
                        case 'max' :
                            if (!$this->checkMax($this->fields[$key], $criterias[1])) {
                                $this->errors[$key][] = sprintf('Długość pola nie powinna przekraczać %s znaków', $criterias[1]);
                            }
                        break;
                        case 'min' :
                            if (!$this->checkMin($this->fields[$key], $criterias[1])) {
                                $this->errors[$key][] = sprintf('Długość pola powinna przekraczać %s znaków', $criterias[1]);
                            }
                        break;
                        case 'match' :
                            if (!$this->checkMatch($this->fields[$key], $criterias[1])) {
                                $this->errors[$key][] = 'Nieprawidłowa wartość!';
                            }
                        break;
                    }
                } else {
                    switch ($criterias) {
                        case 'required' :
                            if (!$this->checkRequired($this->fields[$key])) {
                                $this->errors[$key][] = 'Wypełnij wymagane pole!';
                            }
                        break;
                        case 'alpha' :
                            if (!$this->checkAlpha($this->fields[$key])) {
                                $this->errors[$key][] = 'Nieprawidłowa wartość!';
                            }
                        break;
                        case 'alpha_num' :
                            if (!$this->checkAlphaNum($this->fields[$key])) {
                                $this->errors[$key][] = 'Nieprawidłowa wartość!';
                            }
                        break;
                        case 'num' :
                            if (!$this->checkNum($this->fields[$key])) {
                                $this->errors[$key][] = 'Nieprawidłowa wartość!';
                            }
                        break;
                        case 'email' :
                            if (!$this->checkEmail($this->fields[$key])) {
                                $this->errors[$key][] = 'Nieprawidłowy adres e-mail!';
                            }
                        break;
                    }
                }
            }
        }
    }
}