<aside class="finish">
    <h4><strong>Twoje zamówienie zostało zarejestrowane!</strong></h4>
    <p>Na Twój adres zostały wysłane szczegóły zamówienia! Niebawem skontaktuje się z Tobą nasz support w celu finalizacji transakcji!</p>

    <a href="<?php echo esc_url(home_url('/')); ?>" class="button return pull-left">Powrót do strony</a>
</aside>