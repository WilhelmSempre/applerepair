<?php

    /** @var Apple $apple */
    $apple = Apple::getInstance();

    /** @var Basket $basket */
    $basket = $apple->factory('basket');

    /* Action */
    $basket->productsListAction();

    $productsList = $basket->readBasket();

    $summaryPrice = 0;

    $currency = get_option('currency');
?>

<aside class="table-responsive">
    <form action="" method="post">
        <table class="products-list table table-striped">
            <tr>
                <th class="text-center"><input type="checkbox" name="ids[]" class="all action" value="0"></th>
                <th class="text-center">Usługa</th>
                <th class="text-center">Cena</th>
            </tr>
            <?php
            if (count($productsList) > 0) {
                foreach ($productsList as $product) {
                    ?>
                    <tr>
                        <td class="text-center"><input type="checkbox" class="action" name="ids[]" value="<?php echo $product['id']; ?>"></td>
                        <td><a href="<?php echo get_the_permalink($product['id']); ?>"><?php echo $product['title']; ?></a></td>
                        <td class="text-center"><?php echo $product['price'] . ' ' . $currency; ?></td>
                    </tr>
                    <?php

                    $summaryPrice += $product['price'];
                }
            } else { ?>
                <tr>
                    <td class="text-center" colspan="6">Koszyk jest pusty</td>
                </tr>
            <?php }
            ?>
        </table>
        <div class="pull-left action-panel">
            Akcja:
            <select name="selected-action-list">
                <option value="delete">Usuń zaznaczone</option>
            </select>
            <input class="button" type="submit" name="do-action" value="Wykonaj">
        </div>
        <div class="pull-right summary">
            <?php if ($basket->getProductsCount() !== 0) { ?>
                <a class="button confirm pull-right" href="<?php echo get_permalink(get_page_by_title('Podsumowanie zamówienia')); ?>">Zatwierdź zamówienie</a>
            <?php } ?>
            <p class="pull-right">Do zapłaty: <span class="price"><strong><?php echo $summaryPrice . ' ' . $currency; ?></strong></span></p>
        </div>
    </form>
</aside>