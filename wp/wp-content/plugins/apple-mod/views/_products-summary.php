<?php

/** @var Apple $apple */
$apple = Apple::getInstance();

/** @var Basket $basket */
$basket = $apple->factory('basket');

$fields = [
    'email' => '',
    'name' => '',
    'surname' => '',
    'street' => '',
    'number' => '',
    'local' => '',
    'zip' => '',
    'city' => '',
];

/** @var Validation $validation */
$validation = $apple->factory('validation');

if (isset($_POST['confirm'])) {

    $criterium = [
        'email' => 'required|email',
        'name' => 'required|min:2|alpha',
        'surname' => 'required|min:2|alpha',
        'street' => 'required|min:2|alpha_num',
        'number' => 'required|num',
        'local' => 'num',
        'zip' => 'required|match:/[0-9]{2}-[0-9]{3}/',
        'city' => 'required|min:2|alpha'
    ];

    $fields = [
        'email' => $_POST['data']['email'],
        'name' => $_POST['data']['name'],
        'surname' => $_POST['data']['surname'],
        'street' => $_POST['data']['street'],
        'number' => $_POST['data']['number'],
        'local' => $_POST['data']['local'],
        'zip' => $_POST['data']['zip'],
        'city' => $_POST['data']['city']
    ];

    $validation->setCriterium($criterium)
        ->setFields($fields)
        ->validate();

    if (!$validation->isError()) {

        /** @var Order $order */
        $order = $apple->factory('order');

        $order->setCustomer($fields)
            ->setProducts($basket->readBasket())
            ->saveOrder();

        $basket->deleteBasket();

        /** @var Mailer $mailer */
        $mailer = $apple->factory('mailer');

        $mailContent = $mailer->setMailsPath(PLUGIN_INC_DIR . 'mails.php')
            ->getMailContent('order');

        $variablesToMail = [
            '{email}',
            '{name}',
            '{surname}',
            '{street}',
            '{number}',
            '{local}',
            '{zip}',
            '{city}',
            '{oid}'
        ];

        $fields['oid'] = $order->getLastOrderID();

        $mailContent['content'] = str_replace($variablesToMail, $fields, $mailContent['content']);

        $mailer->setEmailData([
            'reciver' => get_option('email'),
            'requester' => $fields['email'],
            'subject' => $mailContent['subject'],
            'content' => $mailContent['content']
        ])
            ->setHTML(true)
            ->send();

        wp_redirect(get_the_permalink(get_page_by_title('Zamówienie zarejestrowane')));
        exit();
    }
}

if ($basket->getProductsCount() === 0) {

    ?>

    <p class="alert alert-danger" role="alert">Brak zamówień w koszyku!</p>

    <?php
} else { ?>
    <form class="confirm-form col-md-12 col-xs-12 no-margin no-padding" action="" method="post">
        <aside class="col-md-6 col-xs-12">
            <div class="form-group">
                <label>Adres email:</label>
                <?php if ($validation->isError('email')) {
                    ?>
                    <p class="alert alert-danger" role="alert"><?php echo $validation->getErrors('email')[0]; ?></p>
                <?php } ?>
                <input class="form-control" type="email" name="data[email]" placeholder="Wpisz e-mail" value="<?php echo $fields['email']; ?>">
            </div>
            <div class="form-group">
                <label>Imię:</label>
                <?php if ($validation->isError('name')) {
                    ?>
                    <p class="alert alert-danger" role="alert"><?php echo $validation->getErrors('name')[0]; ?></p>
                <?php } ?>
                <input class="form-control" type="text" name="data[name]" placeholder="Wpisz imię" value="<?php echo $fields['name']; ?>">
            </div>
            <div class="form-group">
                <label>Nazwisko:</label>
                <?php if ($validation->isError('surname')) {
                    ?>
                    <p class="alert alert-danger" role="alert"><?php echo $validation->getErrors('surname')[0]; ?></p>
                <?php } ?>
                <input class="form-control" type="text" name="data[surname]" placeholder="Wpisz nazwisko" value="<?php echo $fields['surname']; ?>">
            </div>
            <div class="form-group">
                <label>Ulica:</label>
                <?php if ($validation->isError('street')) {
                    ?>
                    <p class="alert alert-danger" role="alert"><?php echo $validation->getErrors('street')[0]; ?></p>
                <?php } ?>
                <input class="form-control" type="text" name="data[street]" placeholder="Wpisz ulicę" value="<?php echo $fields['street']; ?>">
            </div>
        </aside>
        <aside class="col-md-6 col-xs-12">
            <div class="form-group">
                <label>Numer budynku:</label>
                <?php if ($validation->isError('number')) {
                    ?>
                    <p class="alert alert-danger" role="alert"><?php echo $validation->getErrors('number')[0]; ?></p>
                <?php } ?>
                <input class="form-control" type="text" name="data[number]" placeholder="Wpisz numer budynku" value="<?php echo $fields['number']; ?>">
            </div>
            <div class="form-group">
                <label>Numer lokalu:</label>
                <?php if ($validation->isError('local')) {
                    ?>
                    <p class="alert alert-danger" role="alert"><?php echo $validation->getErrors('local')[0]; ?></p>
                <?php } ?>
                <input class="form-control" type="text" name="data[local]" placeholder="Wpisz numer lokalu" value="<?php echo $fields['local']; ?>">
            </div>
            <div class="form-group">
                <label>Kod pocztowy:</label>
                <?php if ($validation->isError('zip')) {
                    ?>
                    <p class="alert alert-danger" role="alert"><?php echo $validation->getErrors('zip')[0]; ?></p>
                <?php } ?>
                <input class="form-control" type="text" name="data[zip]" placeholder="Wpisz kod pocztowy" value="<?php echo $fields['zip']; ?>">
            </div>
            <div class="form-group">
                <label>Miasto:</label>
                <?php if ($validation->isError('city')) {
                    ?>
                    <p class="alert alert-danger" role="alert"><?php echo $validation->getErrors('city')[0]; ?></p>
                <?php } ?>
                <input class="form-control" type="text" name="data[city]" placeholder="Wpisz miasto" value="<?php echo $fields['city']; ?>">
            </div>
        </aside>
        <aside class="col-xs-12">
            <div class="form-group">
                <a href="<?php echo get_permalink(get_page_by_title('Twój koszyk')); ?>" class="button pull-left">Powrót do koszyka</a>
                <input class="button pull-right" type="submit" name="confirm" value="Złóż zamówienie">
            </div>
        </aside>
    </form>
<?php }
?>
