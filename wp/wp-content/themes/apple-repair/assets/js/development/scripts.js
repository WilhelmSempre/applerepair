/**
 * @author Rafał Głuszak <rafal@reedy.pl>
 */


(function($) {
    'use strict';


    var mr = mr || {};

    mr = {

        createSubmenu: function() {

            var submenu = $('.submenu ul.submenu-item');

            $('.menu li a').each(function(index) {
                if (typeof submenu.eq(index) != 'undefined') {
                    submenu.eq(index).attr('data-parent', $(this).text());
                }
            });

            return mr;
        },

        displaySubmenu: function () {

            $('.menu li a.submenu-handler').on('click', function (event) {
                    event = event || window.event;

                    var parentData = $(this).data('parent'),
                    submenu = $('.submenu'),
                    submenuList = submenu.find('ul[data-parent="' + parentData + '"]'),
                    submenuVisible = submenu.find('ul:visible'),
                    submenuVisibleData = submenuVisible.data('parent');

                if (!submenu.is(':visible')) {
                    submenuList.show(0, function () {
                        submenu.slideDown(200);
                    });
                } else if (parentData != submenuVisibleData) {
                    submenuVisible.hide();
                    submenu.hide();

                    submenuList.show(0, function () {
                        submenu.slideDown(200);
                    });
                } else {
                    submenu.slideUp(200, function () {
                        submenuList.hide(0);
                    });
                }

                event.preventDefault();
            });

            return mr;
        },

        displaySubmenuSubitems: function() {
            var subitem = $('.subitem'),
                subitemChildren = subitem.children('.subitems');

            subitem.children('a').on('click', function(event) {
                event = event || window.event;

                var href = $(this).attr('href');

                if (href === '#' || href === '') {
                    event.preventDefault();
                }
            });

            if (subitemChildren.length !== 0) {
                subitem.on('mouseenter', function() {
                    var currentSubitems = $(this).children('.subitems');

                    if (!currentSubitems.is(':animated')) {
                        subitemChildren.hide(0);
                        $(this).children('.subitems').stop().show();
                    }

                }).on('mouseleave', function() {
                    var currentSubitems = $(this).children('.subitems');

                    if (!currentSubitems.is(':animated')) {
                        subitemChildren.hide(0);
                        $(this).children('.subitems').stop().hide();
                    }
                });
            }

            return mr;
        },

        fancybox: function () {

            var windowHeight = $(window).height(),
                windowWeight = $(window).width();


            $('.fancybox').fancybox({
                minWidth: windowWeight - 2 * 45, /* WilhelmSempre magic meter */
                minHeight: windowHeight - 2 * 45, /* WilhelmSempre magic meter */
                openEffect: 'none',
                closeEffect: 'none',
                title: false,
                type: 'ajax',
                href: $(this).attr('href')
            });

            return mr;
        },

        init: function () {
            mr.createSubmenu()
                .displaySubmenu()
                .displaySubmenuSubitems()
                .fancybox()
                .diagnosticTool();

            $('.sticky').sticky({
                topSpacing: 0
            });

            $(window).on('resize', function() {
                $('.sticker').unstick().sticky({
                    topSpacing: 0
                });
            });


            /* Rotator */
            var tab = $('a[data-toggle="tab"]');

            tab.eq(0).addClass('active');

            tab.on('shown.bs.tab', function (event) {
                tab.removeClass('active');
                $(this).addClass('active');
            });

            /* Hamburger */
            $('.hamburger').click(function(event){
                event = event || window.event;

                var stickyWrapperMenu = $('.sticky-wrapper');

                $(this).toggleClass('open');

                if (!stickyWrapperMenu.is(':visible')) {
                    stickyWrapperMenu.stop(true, true).show();
                } else {
                    stickyWrapperMenu.stop(true, true).hide();
                }

                event.preventDefault();
            });

            return mr;
        },

        diagnosticTool : function() {
            var diagnosticBlock = $('section.tool'),
                startButton = diagnosticBlock.find('.diagnostic-button.start'),
                stepButton = diagnosticBlock.find('.step'),
                endButton = diagnosticBlock.find('.diagnostic-button.end'),
                categoryButton = diagnosticBlock.find('.problem-category').children('a');

            startButton.on('click', function(event) {
                event = event || window.event;

                diagnosticBlock.find('.diagnostic-tool').show();

                event.preventDefault();
            });

            categoryButton.on('click', function(event) {
                event = event || window.event;

                var href = $(this).attr('href');

                if (href === '#' || href === '') {
                    event.preventDefault();
                }
            });

            stepButton.on('click', function(event) {
                event = event || window.event;

                var category = $(this).data('category'),
                    activeItem = diagnosticBlock.find('.item.active');

                activeItem.removeClass('active');
                diagnosticBlock.find('.item.' + category).addClass('active');

                event.preventDefault();
            });

            endButton.on('click', function(event) {
                event = event || window.event;

                var activeItem = diagnosticBlock.find('.item.active');

                diagnosticBlock.find('.diagnostic-tool').hide();
                activeItem.removeClass('active');
                diagnosticBlock.find('.item').eq(0).addClass('active');

                event.preventDefault();
            });

            return mr;
        }
    };

    $(document).ready(function () {
        new mr.init();
    });

})(jQuery);
