<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header">
        <h3 class="entry-title"><?php the_title(); ?></h3>
    </header>

    <div class="entry-content">
        <?php the_content(); ?>
    </div>
    <footer class="entry-meta">

    </footer>
</article>