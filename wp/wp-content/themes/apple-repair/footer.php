<footer>
    <section class="container">
        <div class="col-md-4 col-xs-12">
            <h4><strong>Szybkie linki</strong></h4>
            <ul class="links col-xs-12 list-unstyled no-padding no-margin">
                <?php wp_nav_menu([
                    'theme_location' => 'footer',
                    'container' => false,
                    'items_wrap' => '%3$s',
                    'walker' => new MB_Footer_Menu()
                ]); ?>
            </ul>
        </div>
        <div class="col-md-4 col-xs-12">
            <h4 class="text-uppercase"><strong>Naprawa IPHONE</strong></h4>
            <ul class="links col-xs-12 list-unstyled no-padding no-margin">
                <?php wp_nav_menu([
                    'theme_location' => 'iphone',
                    'container' => false,
                    'items_wrap' => '%3$s',
                    'walker' => new MB_Footer_Menu()
                ]); ?>
            </ul>
        </div>
        <div class="col-md-4 col-xs-12">
            <h4 class="text-uppercase"><strong>Naprawa IPAD</strong></h4>
            <ul class="links col-xs-12 list-unstyled no-padding no-margin">
                <?php wp_nav_menu([
                    'theme_location' => 'ipad',
                    'container' => false,
                    'items_wrap' => '%3$s',
                    'walker' => new MB_Footer_Menu()
                ]); ?>
            </ul>
        </div>
        <section class="copyright col-md-10 col-xs-12 text-left">Wszelkie prawa zastrzezone. <strong><?php echo get_bloginfo('name'); ?></strong> @2015-<?php echo date('Y'); ?></section>
        <aside class="social col-md-2 col-xs-12 pull-right">
            <a href="#" class="col-xs-4 text-center">
                <svg><use xlink:href="#shape-facebook"></use></svg>
            </a>
            <a href="#" class="col-xs-4 text-center">
                <svg><use xlink:href="#shape-twitter"></use></svg>
            </a>
            <a href="#" class="col-xs-4 text-center">
                <svg><use xlink:href="#shape-google"></use></svg>
            </a>
        </aside>
    </section>
</footer>

<?php wp_footer(); ?>
</body>
</html>