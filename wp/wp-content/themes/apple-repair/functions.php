<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

/**
 * Set content width.
 */

if (!isset($content_width)) {
    $content_width = 660;
}

/**
 * Template settings.
 */

if (!function_exists('mb_setup')) {

    /**
     *
     */
    function mb_setup()
    {

        load_theme_textdomain('mb', get_template_directory() . '/languages');

        add_theme_support('automatic-feed-links');
        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');
        set_post_thumbnail_size(825, 510, true);

        add_theme_support('html5', [
            'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
        ]);

        add_theme_support('post-formats', [
            'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
        ]);

        /* Menus */
        register_nav_menu('footer', __('Footer Menu', 'mb'));
        register_nav_menu('primary', __('Primary Menu', 'mb'));
        register_nav_menu('repairs', __('Repairs Menu', 'mb'));
        register_nav_menu('services', __('Services Menu', 'mb'));
        register_nav_menu('iphone', __('IPHONE Menu', 'mb'));
        register_nav_menu('ipad', __('IPAD Menu', 'mb'));

        /* Sidebars */
        register_sidebar([
            'name' => 'Left Sidebar',
            'id' => 'left',
            'description' => '',
            'before_widget' => '<div class="widget %2$s left">',
            'after_widget'  => '</div>',
            'before_title'  => '<h4>',
            'after_title'   => '</h4>',
        ]);

        register_sidebar([
            'name' => 'Right Sidebar',
            'id' => 'right',
            'description' => '',
            'before_widget' => '<div class="widget %2$s right">',
            'after_widget'  => '</div>',
            'before_title'  => '<h4>',
            'after_title'   => '</h4>',
        ]);
    }
}

add_action('after_setup_theme', 'mb_setup');

/**
 * Fonts.
 */

/**
 * @return string
 */
function mb_fonts_url()
{
    $fonts_url = '';
    $fonts = [];
    $subsets = 'latin,latin-ext';

    if ('off' !== _x('on', 'Lato font: on or off', 'mb')) {
        $fonts[] = 'Lato:400italic,700italic,400,700';
    }

    if ($fonts) {
        $fonts_url = add_query_arg([
            'family' => urlencode(implode('|', $fonts)),
            'subset' => urlencode($subsets),
        ], 'https://fonts.googleapis.com/css');
    }

    return $fonts_url;
}

/**
 * Scripts.
 */

function mb_scripts()
{
    /* Main */
    wp_enqueue_style('style', get_stylesheet_uri());

    /* Main script */
    wp_enqueue_script('main', get_template_directory_uri() . '/assets/js/dist/main.min.js', [], '1.0.0', true);
}

add_action('wp_enqueue_scripts', 'mb_scripts');

/* Remove junk features */

add_filter('show_admin_bar', '__return_false');
remove_action('admin_print_styles', 'print_emoji_styles');
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('wp_print_styles', 'print_emoji_styles');
remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
remove_filter('the_content_feed', 'wp_staticize_emoji');
remove_filter('comment_text_rss', 'wp_staticize_emoji');
add_action('do_feed', 'fb_disable_feed', 1);
add_action('do_feed_rdf', 'fb_disable_feed', 1);
add_action('do_feed_rss', 'fb_disable_feed', 1);
add_action('do_feed_rss2', 'fb_disable_feed', 1);
add_action('do_feed_atom', 'fb_disable_feed', 1);
add_action('do_feed_rss2_comments', 'fb_disable_feed', 1);
add_action('do_feed_atom_comments', 'fb_disable_feed', 1);
remove_action('wp_head', 'wp_generator');