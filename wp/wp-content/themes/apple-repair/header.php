<!DOCTYPE html>

<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta charset="<?php bloginfo('charset'); ?>">
    <?php wp_head(); ?>
</head>
<body>
<div class="icons"></div>
<header class="nav">
    <section class="container">
        <aside class="col-md-4 col-xs-8 shopping-cart">
            <a href="<?php echo get_permalink(get_page_by_title('Twój koszyk')); ?>">
                <svg class="pull-left"><use xlink:href="#shape-shopcart"></use></svg>
                <p class="pull-left">
                    <?php

                    if (class_exists('Apple')) {

                        /** @var Apple $apple */
                        $apple = Apple::getInstance();

                        /** @var Basket $basket */
                        $basket = $apple->factory('basket');

                        $productsCount = $basket->getProductsCount();

                        echo sprintf('Twój koszyk: <strong>(%s) %s</strong>', $productsCount ,
                            ($productsCount === 1) ? 'zamówienie' : 'zamówień');
                    }

                    ?>
                </p>
            </a>
        </aside>
        <a href="#" class="hamburger">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </a>
        <aside class="social col-md-2 col-xs-12 pull-right">
            <a href="#" class="col-xs-4 text-center">
                <svg><use xlink:href="#shape-facebook"></use></svg>
            </a>
            <a href="#" class="col-xs-4 text-center">
                <svg><use xlink:href="#shape-twitter"></use></svg>
            </a>
            <a href="#" class="col-xs-4 text-center">
                <svg><use xlink:href="#shape-google"></use></svg>
            </a>
        </aside>
    </section>
</header>
<header class="top sticky">
    <section class="container">
        <aside class="logo col-md-3 col-xs-12">
            <a href="<?php echo get_bloginfo('url'); ?>"><img class="img-responsive" alt="<?php echo get_bloginfo('name'); ?>" src="<?php echo get_template_directory_uri(); ?>/assets/img/naprawa.png"></a>
        </aside>
        <aside class="contact pull-right col-md-3 col-xs-12">
            <p class="no-padding no-margin">534 422 225</p>
            <p class="no-padding no-margin">Łódź, ul.Pomorska 77 III piętro</p>
            <p class="no-padding no-margin">Poniedziałek-Piątek: 10-17</p>
        </aside>
        <nav class="menu pull-right col-md-4 col-xs-12">
            <ul class="no-padding no-margin list-unstyled">
                <?php wp_nav_menu([
                    'theme_location' => 'primary',
                    'container' => false,
                    'items_wrap' => '%3$s',
                    'walker' => new MB_Menu()
                ]); ?>
            </ul>
        </nav>
    </section>
    <section class="submenu col-xs-12">
        <section class="container">
            <?php wp_nav_menu([
                'theme_location' => 'repairs',
                'container' => false,
                'items_wrap' => '<ul class="col-xs-12 no-padding no-margin list-unstyled submenu-item">%3$s</ul>',
                'walker' => new MB_Submenu_Menu()
            ]);
            ?>
        </section>
    </section>
</header>