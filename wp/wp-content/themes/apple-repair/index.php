<?php get_header(); ?>

<section class="rotator no-padding no-margin col-xs-12">
    <div id="rotator" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#rotator" data-slide-to="0" class="active"></li>
        </ol>

        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/img_slider.jpg" alt="Repairs">
                <div class="carousel-caption">
                    <h3 class="text-uppercase"><strong>Repairs</strong></h3>
                    <p>Repairs text.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_template_part('partials/_offers'); ?>

<?php get_template_part('partials/_diagnostic_tool'); ?>

<?php get_template_part('partials/_services'); ?>

<?php get_template_part('partials/_why'); ?>

<?php get_template_part('partials/_info'); ?>

<?php get_footer(); ?>