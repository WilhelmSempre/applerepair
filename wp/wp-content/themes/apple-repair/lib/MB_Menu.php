<?php

/**
 * Class MB_Footer_Menu
 */
class MB_Footer_Menu extends Walker_Nav_Menu
{

    /**
     * @param string $output
     * @param int $depth
     * @param array $args
     */
    function start_lvl( &$output, $depth = 0, $args = []) {
        $output .= "";
    }

    /**
     * @param string $output
     * @param object $item
     * @param int $depth
     * @param array $args
     * @param int $id
     */
    function start_el(&$output, $item, $depth = 0, $args = [], $id = 0)
    {
        $item_output = sprintf('<li><a href="%s">%s</a>',
            esc_attr($item->url),
            apply_filters('the_title', $item->title, $item->ID)
        );

        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
    }
}

/**
 * MB_Submenu_Menu
 */
class MB_Submenu_Menu extends Walker_Nav_Menu
{

    /**
     * @param string $output
     * @param int $depth
     * @param array $args
     */
    function start_lvl( &$output, $depth = 0, $args = []) {
        $output .= '<ul class="subitems col-xs-12 list-unstyled no-padding no-margin">';
    }

    /**
     * @param string $output
     * @param object $item
     * @param int $depth
     * @param array $args
     * @param int $id
     */
    function start_el(&$output, $item, $depth = 0, $args = [], $id = 0)
    {
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

        if ($depth !== 0) {
            $item_output = sprintf('<li class="col-xs-12 no-padding no-margin %s"><a href="%s">%s</a>',
                $class_names,
                esc_attr($item->url),
                apply_filters('the_title', $item->title, $item->ID)
            );
        } else {
            $item_output = sprintf('<li class="subitem col-md-2 col-xs-12 no-padding no-margin text-center %s"><a href="%s">%s</a>',
                $class_names,
                esc_attr($item->url),
                apply_filters('the_title', $item->title, $item->ID)
            );
        }

        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
    }
}

/**
 * Class MB_Menu
 */
class MB_Menu extends Walker_Nav_Menu
{

    /**
     * @param string $output
     * @param int $depth
     * @param array $args
     */
    function start_lvl( &$output, $depth = 0, $args = []) {
        $output .= "";
    }

    /**
     * @param string $output
     * @param object $item
     * @param int $depth
     * @param array $args
     * @param int $id
     */
    function start_el(&$output, $item, $depth = 0, $args = [], $id = 0)
    {
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

        $item_output = sprintf('<li><a data-parent="%s" class="col-md-4 col-xs-12 text-center no-padding no-margin %s" href="%s">%s</a></li>',
            $item->title,
            $class_names,
            esc_attr($item->url),
            apply_filters('the_title', $item->title, $item->ID)
        );

        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
    }
}
