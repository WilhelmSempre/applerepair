<?php

/**
 * Template Name: Ajax
 *
 * @package WordPress
 * @subpackage MP
 */

?>

<section class="ajax">
    <div id="content" role="main">

        <?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'content', 'page' ); ?>
            <?php comments_template( '', true ); ?>
        <?php endwhile; ?>

    </div>
</section>