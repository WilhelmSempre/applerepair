<?php

/**
 * Template Name: Repairs
 *
 * @package WordPress
 * @subpackage MP
 */

get_header();

$id = get_the_ID();

        /** @var Apple $apple */
        $apple = Apple::getInstance();

        /** @var Basket $basket */
        $basket = $apple->factory('basket');

        $price = get_post_meta($id, 'Cena', true);
        $title = get_the_title();

        if (isset($_POST['order'])) {
            $basket->addProduct([
                'title' => $title,
                'price' => $price,
                'id' => $id
            ]);

            wp_redirect(get_the_permalink(), 302);
        }

?>

            <section class="container offer-page">
                <div id="content" role="main">

                    <?php while (have_posts()) : the_post(); ?>
                        <?php get_template_part('content', 'page'); ?>
                        <?php comments_template('', true); ?>
                    <?php endwhile; ?>

                </div>
            </section>


<?php 
    get_footer();
?>
