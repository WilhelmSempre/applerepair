<?php

/**
 * Template Name: 2 column
 *
 * @package WordPress
 * @subpackage MP
 */

get_header();

?>

<section class="container">
    <aside class="sidebar left col-md-3 col-xs-12">
        <?php dynamic_sidebar('left'); ?>
    </aside>

    <aside class="page col-md-6 col-xs-12">
        <div id="content" role="main">

            <?php while ( have_posts() ) : the_post(); ?>
                <?php get_template_part( 'content', 'page' ); ?>
                <?php comments_template( '', true ); ?>
            <?php endwhile; ?>

        </div>
    </aside>

    <aside class="sidebar right col-md-3 col-xs-12">
        <?php dynamic_sidebar('right'); ?>
    </aside>
</section>

<?php get_footer(); ?>