<section class="tool no-padding no-margin col-xs-12 text-center">
    <section class="container">
        <?php get_template_part('partials/diagnostic_tool/_cover'); ?>
    </section>
    <div class="no-margin no-padding diagnostic-tool col-xs-12">
        <section class="container">
            <div class="device item active">
                <h3>Wybierz swoje urządzenie</h3>
                <?php get_template_part('partials/diagnostic_tool/_device'); ?>
            </div>
            <div class="ipad item">
                <h3>Wybierz model</h3>
                <?php get_template_part('partials/diagnostic_tool/ipad/_ipad'); ?>
            </div>
            <div class="iphone item">
                <h3>Wybierz model</h3>
                <?php get_template_part('partials/diagnostic_tool/iphone/_iphone'); ?>
            </div>
            <div class="iphone3g3gs item">
                <h3>Wybierz problem</h3>
                <?php get_template_part('partials/diagnostic_tool/iphone/iphone3g3gs/_iphone3g3gs'); ?>
            </div>
            <div class="iphone4 item">
                <h3>Wybierz problem</h3>
                <?php get_template_part('partials/diagnostic_tool/iphone/iphone4/_iphone4'); ?>
            </div>
            <div class="iphone4s item">
                <h3>Wybierz problem</h3>
                <?php get_template_part('partials/diagnostic_tool/iphone/iphone4s/_iphone4s'); ?>
            </div>
            <div class="iphone5 item">
                <h3>Wybierz problem</h3>
                <?php get_template_part('partials/diagnostic_tool/iphone/iphone5/_iphone5'); ?>
            </div>
            <div class="iphone5c item">
                <h3>Wybierz problem</h3>
                <?php get_template_part('partials/diagnostic_tool/iphone/iphone5c/_iphone5c'); ?>
            </div>
            <div class="iphone5s item">
                <h3>Wybierz problem</h3>
                <?php get_template_part('partials/diagnostic_tool/iphone/iphone5s/_iphone5s'); ?>
            </div>
            <div class="iphone6 item">
                <h3>Wybierz problem</h3>
                <?php get_template_part('partials/diagnostic_tool/iphone/iphone6/_iphone6'); ?>
            </div>
            <div class="iphone6+ item">
                <h3>Wybierz problem</h3>
                <?php get_template_part('partials/diagnostic_tool/iphone/iphone6+/_iphone6+'); ?>
            </div>
            <div class="ipad2 item">
                <h3>Wybierz problem</h3>
                <?php get_template_part('partials/diagnostic_tool/ipad/ipad2/_ipad2'); ?>
            </div>
            <div class="ipad3 item">
                <h3>Wybierz problem</h3>
                <?php get_template_part('partials/diagnostic_tool/ipad/ipad3/_ipad3'); ?>
            </div>
            <div class="ipad4 item">
                <h3>Wybierz problem</h3>
                <?php get_template_part('partials/diagnostic_tool/ipad/ipad4/_ipad4'); ?>
            </div>
            <div class="ipadair item">
                <h3>Wybierz problem</h3>
                <?php get_template_part('partials/diagnostic_tool/ipad/ipadair/_ipadair'); ?>
            </div>
            <div class="ipadmini item">
                <h3>Wybierz problem</h3>
                <?php get_template_part('partials/diagnostic_tool/ipad/ipadmini/_ipadmini'); ?>
            </div>
            <div class="ipadmini2 item">
                <h3>Wybierz problem</h3>
                <?php get_template_part('partials/diagnostic_tool/ipad/ipadmini/_ipadmini2'); ?>
            </div>
        </section>
        <a class="end diagnostic-button" href="#">Zakończ</a>
    </div>
</section>