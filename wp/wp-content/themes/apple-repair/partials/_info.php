<section class="info no-padding no-margin col-xs-12 text-center">
    <section class="container">
        <h3><strong>Nie możesz znaleźć czego potrzebujesz?</strong></h3>
        <aside class="col-md-4 text-left">
            <svg class="pull-left">
                <use xlink:href="#shape-mail"></use>
            </svg>
            <h5><strong>Email</strong></h5>
            <p>Zwykle odpowiadamy do 24H</p>
            <a href="mailto:" class="button">Kliknij aby wysłać email!</a>
        </aside>
        <aside class="col-md-4 text-left">
            <svg class="pull-left">
                <use xlink:href="#shape-phone"></use>
            </svg>
            <h5><strong>Zadzwoń do nas</strong></h5>
            <p>Zadzwoń do nas by dowiedzieć się więcej.</p>
            <a href="mailto:" class="button"><strong>534 422 225</strong></a>
        </aside>
        <aside class="col-md-4 text-left">
            <svg class="pull-left">
                <use xlink:href="#shape-chat"></use>
            </svg>
            <h5><strong>Livechat</strong></h5>
            <p>Kliknij, a jeden z naszych konsultantów zostanie przydzielony do Ciebie.</p>
            <a class="button">Livechat</a>
        </aside>
    </section>
</section>
