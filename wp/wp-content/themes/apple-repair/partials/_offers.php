<section class="offers no-padding no-margin col-xs-12">
    <section class="container">
        <aside class="col-md-4 col-xs-12">
            <a class="fancybox" href="<?php echo get_permalink(66); ?>">
                <svg class="pull-left"><use xlink:href="#shape-delivery"></use></svg>
                <h4><strong>Darmowa dostawa</strong></h4>
                <p>Dowiedz się więcej</p></a>
        </aside>
        <aside class="col-md-4 col-xs-12">
            <a class="fancybox" href="<?php echo get_permalink(64); ?>">
                <svg class="pull-left"><use xlink:href="#shape-basket"></use></svg>
                <h4><strong>Odbiór w domu</strong></h4>
                <p>Na terenie Łodzi</p></a>
        </aside>
        <aside class="col-md-4 col-xs-12">
            <a class="fancybox" href="<?php echo get_permalink(68); ?>">
                <svg class="pull-left"><use xlink:href="#shape-bank"></use></svg>
                <h4><strong>Zaoszczędź do 30%-50%</strong></h4>
                <p>Sprawdź promocje</p></a>
        </aside>
    </section>
</section>

