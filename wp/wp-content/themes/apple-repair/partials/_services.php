<section class="services no-padding no-margin col-xs-12">
    <section class="container">
        <a class="service active col-md-4 col-xs-12 text-center" aria-controls="repair" role="tab" data-toggle="tab" href="#repair">
            <h4><strong>Co naprawiamy?</strong></h4>
            <p>Zobacz nasze usługi i cenny</p>
        </a>
        <a class="service col-md-4 col-xs-12 text-center" aria-controls="service" role="tab" data-toggle="tab" href="#service">
            <h4><strong>Co zyskujesz?</strong></h4>
            <p>Gwarancja i informacje dodatkowe</p>

        </a>
        <a class="service col-md-4 col-xs-12 text-center" aria-controls="order" role="tab" data-toggle="tab" href="#order">
            <h4><strong>Ponad 1000 napraw</strong></h4>
            <p>Zobacz więcej</p>
        </a>
    </section>
</section>

<section class="services-wrapper no-padding no-margin col-xs-12 text-center tab-content">
    <section role="tabpanel" id="repair" class="container tab-pane fade in active">
        <h4><strong>At repair the customers always come first!</strong></h4>
        <p class="description">We offer first class service with repair!</p>

        <aside class="devices col-xs-12">
            <div class="device-group">
                <a class="device col-md-2 col-xs-4 text-center" href="<?php echo get_permalink(get_page_by_title('IPHONE', OBJECT, 'offer')); ?>">
                    <svg><use xlink:href="#shape-iphone"></use></svg>
                    <p>IPHONE</p>
                </a>
                <a class="device col-md-2 col-xs-4 text-center" href="<?php echo get_permalink(get_page_by_title('IPAD', OBJECT, 'offer')); ?>">
                    <svg><use xlink:href="#shape-ipad"></use></svg>
                    <p>IPAD</p>
                </a>
                <a class="device col-md-2 col-xs-4 text-center" href="http://apple.how2play.pl/repairs/macbooks/">
                    <svg><use xlink:href="#shape-monitor"></use></svg>
                    <p>Macbooks</p>
                </a>
                <a class="device col-md-2 col-xs-4 text-center" href="http://apple.how2play.pl/repairs/computers/">
                    <svg><use xlink:href="#shape-mac"></use></svg>
                    <p>Computers</p>
                </a>
                <a class="device col-md-2 col-xs-4 text-center" href="<?php echo get_permalink(get_page_by_title('IPOD', OBJECT, 'offer')); ?>">
                    <svg><use xlink:href="#shape-ipad"></use></svg>
                    <p>IPOD</p>
                </a>
                <a class="device col-md-2 col-xs-4 text-center" href="http://apple.how2play.pl/repairs/others/">
                    <svg><use xlink:href="#shape-others"></use></svg>
                    <p>INNE</p>
                </a>
            </div>
        </aside>
    </section>
    <section role="tabpanel" id="service" class="container fade tab-pane">
        <h4><strong></strong></h4>
        <p class="description"></p>
    </section>
    <section role="tabpanel" id="order" class="container fade tab-pane">
        <h4><strong>At repair the customers always come first!</strong></h4>
        <p class="description">We offer first class service with repair!</p>
    </section>
</section>

