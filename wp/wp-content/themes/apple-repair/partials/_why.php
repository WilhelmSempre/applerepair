<section class="why no-padding no-margin col-xs-12 text-center">
    <section class="container">
        <h3><strong>Czemu powinieneś nas wybrac?</strong></h3>
        <div class="tab-content">
            <div id="fast" class="tab-pane fade in active">
                <h4>Szybka naprawa</h4>
                <p class="description"></p>
            </div>
            <div id="confidence" class="tab-pane fade">
                <h4>Można nam zaufać</h4>
                <p class="description"></p>
            </div>
            <div id="reliable" class="tab-pane fade">
                <h4>Niezawodność</h4>
                <p class="description"></p>
            </div>
            <div id="isolation" class="tab-pane fade">
                <h4>Doświadczenie</h4>
                <p class="description"></p>
            </div>
            <div id="mood" class="tab-pane fade">
                <h4>Zniżki nawet do 75%</h4>
                <p class="description"></p>
            </div>
            <div id="proffesional" class="tab-pane fade">
                <h4>Profesjonalizm</h4>
                <p class="description"></p>
            </div>
        </div>

        <div class="devices">
            <a class="device active col-md-2 col-xs-12 text-center" aria-controls="home" role="tab" data-toggle="tab" href="#fast">
                <svg><use xlink:href="#shape-repair"></use></svg>
                <p>Szybka naprawa</p>
            </a>
            <a class="device col-md-2 col-xs-12 text-center" aria-controls="confidence" role="tab" data-toggle="tab" href="#confidence">
                <svg><use xlink:href="#shape-repair"></use></svg>
                <p>Można nam zaufać</p>
            </a>
            <a class="device col-md-2 col-xs-12 text-center" aria-controls="reliable" role="tab" data-toggle="tab" href="#reliable">
                <svg><use xlink:href="#shape-repair"></use></svg>
                <p>Niezawodność</p>
            </a>
            <a class="device col-md-2 col-xs-12 text-center" aria-controls="isolation" role="tab" data-toggle="tab" href="#isolation">
                <svg><use xlink:href="#shape-repair"></use></svg>
                <p>Doświadczenie</p>
            </a>
            <a class="device col-md-2 col-xs-12 text-center" aria-controls="mood" role="tab" data-toggle="tab" href="#mood">
                <svg><use xlink:href="#shape-repair"></use></svg>
                <p>Zniżki nawet do 75%</p>
            </a>
            <a class="device col-md-2 col-xs-12 text-center" aria-controls="proffesional" role="tab" data-toggle="tab" href="#proffesional">
                <svg><use xlink:href="#shape-repair"></use></svg>
                <p>Profesjonalizm</p>
            </a>
        </div>
    </section>
</section>

