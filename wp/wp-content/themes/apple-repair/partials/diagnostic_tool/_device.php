<div class="device col-md-6 col-xs-12">
    <a data-category="iphone" class="step iphone col-xs-12 text-center" href="#">
        <svg><use xlink:href="#shape-iphone"></use></svg>
        <p>IPHONE</p>
    </a>
</div>

<div class="device col-md-6 col-xs-12">
    <a data-category="ipad" class="step ipad col-xs-12 text-center" href="#">
        <svg><use xlink:href="#shape-ipad"></use></svg>
        <p>IPAD</p>
    </a>
</div>
