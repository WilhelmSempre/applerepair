<div class="device col-md-2 col-xs-12">
    <a data-category="ipad2" class="step iphone col-xs-12 text-center" href="#">
        <svg><use xlink:href="#shape-ipad"></use></svg>
        <p>IPAD 2</p>
    </a>
</div>

<div class="device col-md-2 col-xs-12">
    <a data-category="ipad3" class="step iphone col-xs-12 text-center" href="#">
        <svg><use xlink:href="#shape-ipad"></use></svg>
        <p>IPAD 3</p>
    </a>
</div>

<div class="device col-md-2 col-xs-12">
    <a data-category="ipad4" class="step iphone col-xs-12 text-center" href="#">
        <svg><use xlink:href="#shape-ipad"></use></svg>
        <p>IPAD 4</p>
    </a>
</div>

<div class="device col-md-2 col-xs-12">
    <a data-category="ipadair" class="step iphone col-xs-12 text-center" href="#">
        <svg><use xlink:href="#shape-ipad"></use></svg>
        <p>IPAD AIR</p>
    </a>
</div>

<div class="device col-md-2 col-xs-12">
    <a data-category="ipadmini" class="step iphone col-xs-12 text-center" href="#">
        <svg><use xlink:href="#shape-ipad"></use></svg>
        <p>IPAD MINI</p>
    </a>
</div>

<div class="device col-md-2 col-xs-12">
    <a data-category="ipadmini2" class="step iphone col-xs-12 text-center" href="#">
        <svg><use xlink:href="#shape-ipad"></use></svg>
        <p>IPAD MINI 2</p>
    </a>
</div>

<div class="col-xs-12">
    <a data-category="device" class="step diagnostic-button" href="#">Back</a>
</div>