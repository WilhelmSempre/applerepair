<div class="problem-category col-md-2 col-xs-12">
    <a href="#">
        <svg><use xlink:href="#shape-screen"></use></svg>
        <p>Wyświetlacz</p>
    </a>
    <ul class="list no-padding no-margin list-unstyled">
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa wyświetlacza', 'OBJECT', 'offer')->ID); ?>">Twój wyświetlacz ma "bąbelki"</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa wyświetlacza', 'OBJECT', 'offer')->ID); ?>">Twój wyświetlacz jest rozbity</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa wyświetlacza', 'OBJECT', 'offer')->ID); ?>">Dotyk nie działa</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa płyty głównej', 'OBJECT', 'offer')->ID); ?>">Dotyk nie działa w określonych miejscach</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa płyty głównej', 'OBJECT', 'offer')->ID); ?>">Dotyk czasami nie działa</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa wyświetlacza', 'OBJECT', 'offer')->ID); ?>">Wyświetlacz ma czarne pola</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa wyświetlacza', 'OBJECT', 'offer')->ID); ?>">Wyświetlacz jest czarny z niebieskimi poziomymi paskami</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa wyświetlacza', 'OBJECT', 'offer')->ID); ?>">Wyświetlacz nie działa</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa wyświetlacza', 'OBJECT', 'offer')->ID); ?>">Wyświetlacz jest biały</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa wyświetlacza', 'OBJECT', 'offer')->ID); ?>">Dotyk nie działa w określonych miejscach</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa wyświetlacza', 'OBJECT', 'offer')->ID); ?>">Dotyk czasami nie działa</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa wyświetlacza', 'OBJECT', 'offer')->ID); ?>">Wyświetlacz ma czarne pola</a>
        </li>
    </ul>
</div>

<div class="problem-category col-md-2 col-xs-12">
    <a>
        <svg><use xlink:href="#shape-button"></use></svg>
        <p>Przyciski</p>
    </a>
    <ul class="list no-padding no-margin list-unstyled">
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa głównego przycisku', 'OBJECT', 'offer')->ID); ?>">Twój główny przycisk wcale nie działa</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa głównego przycisku', 'OBJECT', 'offer')->ID); ?>">Twój główny przycisk nie działa podczas wciskania</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa przycisku zasilania', 'OBJECT', 'offer')->ID); ?>">Twój przycisk wyłączania wciska się, ale nie działa</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa głównego przycisku', 'OBJECT', 'offer')->ID); ?>">Twój główny przycisk wyłączania jest wciśnięty</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa przycisku zasilania', 'OBJECT', 'offer')->ID); ?>">Dotyk reaguje na wciśnięcie przycisku włączania</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa przycisków głośności', 'OBJECT', 'offer')->ID); ?>">Dotyk reaguje na wciśnięcie przycisku głośności</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa przycisków głośności', 'OBJECT', 'offer')->ID); ?>">Przycisku głośności nie działają</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa przycisków głośności', 'OBJECT', 'offer')->ID); ?>">Nie da się kliknąć przycisków głośności</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa przycisku wyciszania', 'OBJECT', 'offer')->ID); ?>">Przycisk wyciszania nie działa</a>
        </li>
    </ul>
</div>

<div class="problem-category col-md-2 col-xs-12">
    <a href="#">
        <svg><use xlink:href="#shape-power"></use></svg>
        <p>Włączanie, Ładowanie, Bateria</p>
    </a>
    <ul class="list no-padding no-margin list-unstyled">
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa baterii', 'OBJECT', 'offer')->ID); ?>">Bateria szybko się rozładowuje</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa płyty głównej', 'OBJECT', 'offer')->ID); ?>">Bateria się rozładowuje podczas wykonywania połączeń</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa płyty głównej', 'OBJECT', 'offer')->ID); ?>">Bateria się rozładowuje podczas wysyłania pakietów Internet</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa baterii', 'OBJECT', 'offer')->ID); ?>">Bateria jest spuchnięta</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa złącza Lighting', 'OBJECT', 'offer')->ID); ?>">Bateria nie ładuje się do końca</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa złącza Lighting', 'OBJECT', 'offer')->ID); ?>">Bateria się ładuje się przez kilka minut i przestaje</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa złącza Lighting', 'OBJECT', 'offer')->ID); ?>">Bateria się ładuje się tylko gdy urządzenie jest wyłączone</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa złącza Lighting', 'OBJECT', 'offer')->ID); ?>">Bateria się ładuje się tylko do określonego poziomu</a>
        </li>
    </ul>
</div>

<div class="problem-category col-md-2 col-xs-12">
    <a href="#">
        <svg><use xlink:href="#shape-camera"></use></svg>
        <p>Aparat</p>
    </a>
    <ul class="list no-padding no-margin list-unstyled">
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa aparatu', 'OBJECT', 'offer')->ID); ?>">Aparat nie działa</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa aparatu', 'OBJECT', 'offer')->ID); ?>">Aparat wyświetla czarne pola</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa aparatu', 'OBJECT', 'offer')->ID); ?>">Aparat z przodu nie działa</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa aparatu', 'OBJECT', 'offer')->ID); ?>">Aparat z przodu wyświetla czarne pola</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa flash', 'OBJECT', 'offer')->ID); ?>">Flash nie działa</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa flash', 'OBJECT', 'offer')->ID); ?>">Flash słabo świeci</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa flash', 'OBJECT', 'offer')->ID); ?>">Flash jest zawsze włączony</a>
        </li>
    </ul>
</div>

<div class="problem-category col-md-2 col-xs-12">
    <a href="#">
        <svg><use xlink:href="#shape-water"></use></svg>
        <p>Zalany telefon</p>
    </a>
</div>

<div class="problem-category col-md-2 col-xs-12">
    <a href="#">
        <svg><use xlink:href="#shape-others"></use></svg>
        <p>Other problems</p>
    </a>
    <ul class="list no-padding no-margin list-unstyled">
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa wibratora', 'OBJECT', 'offer')->ID); ?>">Wibracje nie działają</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPAD 2 - Naprawa czytnika karty SIM', 'OBJECT', 'offer')->ID); ?>">Telefon nie rozpoznaje karty SIM</a>
        </li>
    </ul>
</div>

<div class="col-xs-12">
    <a data-category="ipad" class="step diagnostic-button" href="#">Back</a>
</div>