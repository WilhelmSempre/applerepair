<div class="device col-md-2 col-xs-12">
    <a data-category="iphone6+" class="step iphone col-xs-12 text-center" href="#">
        <svg><use xlink:href="#shape-iphone"></use></svg>
        <p>IPHONE 6+</p>
    </a>
</div>

<div class="device col-md-2 col-xs-12">
    <a data-category="iphone6" class="step iphone col-xs-12 text-center" href="#">
        <svg><use xlink:href="#shape-iphone"></use></svg>
        <p>IPHONE 6</p>
    </a>
</div>

<div class="device col-md-2 col-xs-12">
    <a data-category="iphone5s" class="step iphone col-xs-12 text-center" href="#">
        <svg><use xlink:href="#shape-iphone"></use></svg>
        <p>IPHONE 5S</p>
    </a>
</div>

<div class="device col-md-2 col-xs-12">
    <a data-category="iphone5c" class="step iphone col-xs-12 text-center" href="#">
        <svg><use xlink:href="#shape-iphone"></use></svg>
        <p>IPHONE 5C</p>
    </a>
</div>

<div class="device col-md-2 col-xs-12">
    <a data-category="iphone5" class="step iphone col-xs-12 text-center" href="#">
        <svg><use xlink:href="#shape-iphone"></use></svg>
        <p>IPHONE 5</p>
    </a>
</div>

<div class="device col-md-2 col-xs-12">
    <a data-category="iphone4s" class="step iphone col-xs-12 text-center" href="#">
        <svg><use xlink:href="#shape-iphone"></use></svg>
        <p>IPHONE 4S</p>
    </a>
</div>

<div class="device col-md-2 col-xs-12">
    <a data-category="iphone4" class="step iphone col-xs-12 text-center" href="#">
        <svg><use xlink:href="#shape-iphone"></use></svg>
        <p>IPHONE 4</p>
    </a>
</div>

<div class="device col-md-2 col-xs-12">
    <a data-category="iphone3g3gs" class="step iphone col-xs-12 text-center" href="#">
        <svg><use xlink:href="#shape-iphone"></use></svg>
        <p>IPHONE 3G/3GS</p>
    </a>
</div>

<div class="col-xs-12">
    <a data-category="device" class="step diagnostic-button" href="#">Back</a>
</div>