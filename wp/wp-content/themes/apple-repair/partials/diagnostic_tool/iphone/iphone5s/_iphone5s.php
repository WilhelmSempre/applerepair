<div class="problem-category col-md-2 col-xs-12">
    <a href="#">
        <svg><use xlink:href="#shape-screen"></use></svg>
        <p>Wyświetlacz</p>
    </a>
    <ul class="list no-padding no-margin list-unstyled">
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa wyświetlacza', 'OBJECT', 'offer')->ID); ?>">Twój wyświetlacz ma "bąbelki"</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa wyświetlacza', 'OBJECT', 'offer')->ID); ?>">Twój wyświetlacz jest rozbity</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa wyświetlacza', 'OBJECT', 'offer')->ID); ?>">Dotyk nie działa</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa płyty głównej', 'OBJECT', 'offer')->ID); ?>">Dotyk nie działa w określonych miejscach</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa płyty głównej', 'OBJECT', 'offer')->ID); ?>">Dotyk czasami nie działa</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa wyświetlacza', 'OBJECT', 'offer')->ID); ?>">Wyświetlacz ma czarne pola</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa wyświetlacza', 'OBJECT', 'offer')->ID); ?>">Wyświetlacz jest czarny z niebieskimi poziomymi paskami</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa wyświetlacza', 'OBJECT', 'offer')->ID); ?>">Wyświetlacz nie działa</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa wyświetlacza', 'OBJECT', 'offer')->ID); ?>">Wyświetlacz jest biały</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa wyświetlacza', 'OBJECT', 'offer')->ID); ?>">Dotyk nie działa w określonych miejscach</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa wyświetlacza', 'OBJECT', 'offer')->ID); ?>">Dotyk czasami nie działa</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa wyświetlacza', 'OBJECT', 'offer')->ID); ?>">Wyświetlacz ma czarne pola</a>
        </li>
    </ul>
</div>

<div class="problem-category col-md-2 col-xs-12">
    <a href="#">
        <svg><use xlink:href="#shape-button"></use></svg>
        <p>Przyciski</p>
    </a>
    <ul class="list no-padding no-margin list-unstyled">
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa głównego przycisku', 'OBJECT', 'offer')->ID); ?>">Twój główny przycisk wcale nie działa</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa głównego przycisku', 'OBJECT', 'offer')->ID); ?>">Twój główny przycisk nie działa podczas wciskania</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa przycisku zasilania', 'OBJECT', 'offer')->ID); ?>">Twój przycisk wyłączania wciska się, ale nie działa</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa głównego przycisku', 'OBJECT', 'offer')->ID); ?>">Twój główny przycisk wyłączania jest wciśnięty</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa przycisku zasilania', 'OBJECT', 'offer')->ID); ?>">Dotyk reaguje na wciśnięcie przycisku włączania</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa przycisków głośności', 'OBJECT', 'offer')->ID); ?>">Dotyk reaguje na wciśnięcie przycisku głośności</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa przycisków głośności', 'OBJECT', 'offer')->ID); ?>">Przycisku głośności nie działają</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa przycisków głośności', 'OBJECT', 'offer')->ID); ?>">Nie da się kliknąć przycisków głośności</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa przycisku wyciszania', 'OBJECT', 'offer')->ID); ?>">Przycisk wyciszania nie działa</a>
        </li>
    </ul>
</div>

<div class="problem-category col-md-2 col-xs-12">
    <a href="#">
        <svg><use xlink:href="#shape-power"></use></svg>
        <p>Włączanie, Ładowanie, Bateria</p>
    </a>
    <ul class="list no-padding no-margin list-unstyled">
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa baterii', 'OBJECT', 'offer')->ID); ?>">Bateria szybko się rozładowuje</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa płyty głównej', 'OBJECT', 'offer')->ID); ?>">Bateria się rozładowuje podczas wykonywania połączeń</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa płyty głównej', 'OBJECT', 'offer')->ID); ?>">Bateria się rozładowuje podczas wysyłania pakietów Internet</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa baterii', 'OBJECT', 'offer')->ID); ?>">Bateria jest spuchnięta</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa złącza Lighting', 'OBJECT', 'offer')->ID); ?>">Bateria nie ładuje się do końca</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa złącza Lighting', 'OBJECT', 'offer')->ID); ?>">Bateria się ładuje się przez kilka minut i przestaje</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa złącza Lighting', 'OBJECT', 'offer')->ID); ?>">Bateria się ładuje się tylko gdy urządzenie jest wyłączone</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa złącza Lighting', 'OBJECT', 'offer')->ID); ?>">Bateria się ładuje się tylko do określonego poziomu</a>
        </li>
    </ul>
</div>

<div class="problem-category col-md-2 col-xs-12">
    <a href="#">
        <svg><use xlink:href="#shape-camera"></use></svg>
        <p>Aparat</p>
    </a>
    <ul class="list no-padding no-margin list-unstyled">
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa aparatu', 'OBJECT', 'offer')->ID); ?>">Aparat nie działa</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa aparatu', 'OBJECT', 'offer')->ID); ?>">Aparat wyświetla czarne pola</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa aparatu', 'OBJECT', 'offer')->ID); ?>">Aparat z przodu nie działa</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa aparatu', 'OBJECT', 'offer')->ID); ?>">Aparat z przodu wyświetla czarne pola</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa flash', 'OBJECT', 'offer')->ID); ?>">Flash nie działa</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa flash', 'OBJECT', 'offer')->ID); ?>">Flash słabo świeci</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa flash', 'OBJECT', 'offer')->ID); ?>">Flash jest zawsze włączony</a>
        </li>
    </ul>
</div>

<div class="problem-category col-md-2 col-xs-12">
    <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Zalany telefon', 'OBJECT', 'offer')->ID); ?>">
        <svg><use xlink:href="#shape-water"></use></svg>
        <p>Zalany telefon</p>
    </a>
</div>

<div class="problem-category col-md-2 col-xs-12">
    <a href="#">
        <svg><use xlink:href="#shape-sound"></use></svg>
        <p>Dzwięk</p>
    </a>
    <ul class="list no-padding no-margin list-unstyled">
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa płyty głównej', 'OBJECT', 'offer')->ID); ?>">Dzwięki nie działają</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa głośnika', 'OBJECT', 'offer')->ID); ?>">Głośnik nie działa</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa głośnika', 'OBJECT', 'offer')->ID); ?>">Dzwięk jest bardzo słaby</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa mikrofonu', 'OBJECT', 'offer')->ID); ?>">Mikrofon nie działa</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa mikrofonu', 'OBJECT', 'offer')->ID); ?>">Mikrofon nie nagrywa poprawnie</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa mikrofonu', 'OBJECT', 'offer')->ID); ?>">Mikrofon nie działa podczas rozmowy</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa głośnika', 'OBJECT', 'offer')->ID); ?>">Earspeaker nie działa</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa głośnika', 'OBJECT', 'offer')->ID); ?>">Earspeaker emituje słaby dzwięk</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa mikrofonu', 'OBJECT', 'offer')->ID); ?>">Nagrywarka nie nagrywa dzwięku</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa słuchawek', 'OBJECT', 'offer')->ID); ?>">Słuchawki nie działają</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa słuchawek', 'OBJECT', 'offer')->ID); ?>">Słuchawki emitują słaby dzwięk</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa słuchawek', 'OBJECT', 'offer')->ID); ?>">Słuchawki nie działają podczas rozmowy</a>
        </li>
    </ul>
</div>

<div class="problem-category col-md-2 col-xs-12">
    <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Reinstalacja oprogramowania', 'OBJECT', 'offer')->ID); ?>">
        <svg><use xlink:href="#shape-software"></use></svg>
        <p>Oprogramowanie</p>
    </a>
</div>

<div class="problem-category col-md-2 col-xs-12">
    <a href="#">
        <svg><use xlink:href="#shape-signal"></use></svg>
        <p>Błędy sygnału (GSM, WIFI, 3G, Bluetooth)</p>
    </a>
    <ul class="list no-padding no-margin list-unstyled">
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa płyty głównej', 'OBJECT', 'offer')->ID); ?>">Sygnał jest słaby</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa płyty głównej', 'OBJECT', 'offer')->ID); ?>">Sygnał jest utracany i powraca na nowo</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa płyty głównej', 'OBJECT', 'offer')->ID); ?>">Jest zasięg, ale nie możesz wykonywać połączeń wychodzących</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa płyty głównej', 'OBJECT', 'offer')->ID); ?>">Urządzenie pokazuje "Brak usługi" cały czas</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa płyty głównej', 'OBJECT', 'offer')->ID); ?>">Urządzenie nie może się połączyć przez 3G/4G</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa płyty głównej', 'OBJECT', 'offer')->ID); ?>">Urządzenie łączy się przez 3G/4G, ale nie odbiera danych</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Odblokowanie SIM', 'OBJECT', 'offer')->ID); ?>">Urządzenie jest podłączone do obcej sieci</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa WI-FI', 'OBJECT', 'offer')->ID); ?>">Zasięg WIFI jest słaby</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa WI-FI', 'OBJECT', 'offer')->ID); ?>">Urządzenie nie może połączyć się z siecią WIFI</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa WI-FI', 'OBJECT', 'offer')->ID); ?>">Bluetooth nie działa</a>
        </li>
    </ul>
</div>

<div class="problem-category col-md-2 col-xs-12">
    <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa płyty głównej', 'OBJECT', 'offer')->ID); ?>">
        <svg><use xlink:href="#shape-dead"></use></svg>
        <p>Zepsute urządzenie</p>
    </a>
</div>

<div class="problem-category col-md-2 col-xs-12">
    <a href="#">
        <svg><use xlink:href="#shape-others"></use></svg>
        <p>Inne problemy</p>
    </a>
    <ul class="list no-padding no-margin list-unstyled">
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa wibratora', 'OBJECT', 'offer')->ID); ?>">Wibracje nie działają</a>
        </li>
        <li>
            <a href="<?php echo get_permalink(get_page_by_title('IPHONE 5S - Naprawa czytnika karty SIM', 'OBJECT', 'offer')->ID); ?>">Telefon nie rozpoznaje karty SIM</a>
        </li>
    </ul>
</div>

<div class="col-xs-12">
    <a data-category="iphone" class="step diagnostic-button" href="#">Wstecz</a>
</div>