<?php
    get_header();

    $id = get_the_ID();

    $parentCategoryID = wp_get_post_parent_id($id);
    $categoryDepth = count(get_post_ancestors($id));

    $params = [
        'post_parent' => $parentCategoryID !== 0 && $categoryDepth > 1 ? $parentCategoryID : $id,
        'post_type' => 'offer'
    ];

    $currency = get_option('currency');

    $anotherServices = get_children($params);

    if ($parentCategoryID === 0 || $categoryDepth < 2) {
        ?>
            <section class="container offer-page">
                <h3 class="entry-title"><?php echo get_the_title(); ?></h3>

                <section class="content">
                    <?php echo get_the_content(); ?>
                </section>
                <?php

                    /** @var WP_Post $anotherServicePage */
                    foreach ($anotherServices as $anotherServicePage) {

                        $price = get_post_meta($anotherServicePage->ID, 'Cena', true);
                        $realization = get_post_meta($anotherServicePage->ID, 'Czas realizacji', true);
                        $guarantee = get_post_meta($anotherServicePage->ID, 'Gwarancja', true);
                        ?>
                        <a href="<?php echo get_the_permalink($anotherServicePage->ID); ?>">
                            <div class="service col-md-3 col-xs-12">
                                <div class="entry-overlay">
                                    <h4 class="text-center"><strong><?php echo $anotherServicePage->post_title; ?></strong></h4>
                                    <?php echo $anotherServicePage->post_excerpt; ?>
                                </div>
                                <h4 class="text-center"><strong><?php echo wp_trim_words($anotherServicePage->post_title, 6, '...'); ?></strong></h4>
                                <figure class="text-center">
                                    <?php
                                    echo get_the_post_thumbnail($anotherServicePage->ID, [160, 160], [
                                        'class' => 'image-responsive'
                                    ]); ?>
                                </figure>
                                <?php if ($categoryDepth == 1) { ?>

                                <table class="col-xs-12">
                                    <tr>
                                        <th>
                                            <div class="pull-left">
                                                <svg>
                                                    <use xlink:href="#shape-price"></use>
                                                </svg>
                                            </div>
                                            <p class="no-margin pull-left">Cena:</p>
                                        </th>
                                        <td class="text-right">
<?php if ((int) $price === -1) { ?>
                                            <p class="no-margin pull-right">Wycena</p>
<?php } else { ?>
<p class="no-margin pull-right"><?php echo $price . ' ' . $currency; ?></p>
<?php } ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            <div class="pull-left">
                                                <svg>
                                                    <use xlink:href="#shape-time"></use>
                                                </svg>
                                            </div>
                                            <p class="no-margin pull-left">Czas realizacji:</p>
                                        </th>
                                        <td class="text-right">
                                            <p class="no-margin pull-right"><?php echo $realization; ?></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            <div class="pull-left">
                                                <svg>
                                                    <use xlink:href="#shape-guarantee"></use>
                                                </svg>
                                            </div>
                                            <p class="no-margin pull-left">Gwarancja:</p>
                                        </th>
                                        <td class="text-right">
                                            <p class="no-margin pull-right"><?php echo $guarantee; ?></p>
                                        </td>
                                    </tr>
                                </table>
                                <?php } ?>
                            </div>
                        </a>
                        <?php
                    }
                ?>
            </section>
        <?php
    } else {

        /** @var Apple $apple */
        $apple = Apple::getInstance();

        /** @var Basket $basket */
        $basket = $apple->factory('basket');

        $price = get_post_meta($id, 'Cena', true);
        $realization = get_post_meta($id, 'Czas realizacji', true);
        $guarantee = get_post_meta($id, 'Gwarancja', true);
        $title = get_the_title();

        if (isset($_POST['order'])) {
            $basket->addProduct([
                'title' => $title,
                'price' => $price,
                'id' => $id
            ]);

            wp_redirect(get_the_permalink(), 302);
        }

        ?>
        <section class="container offer-page">
            <aside class="sidebar left col-md-3 col-xs-12">
                <div class="widget info-box pull-left col-xs-12">
                    <figure class="text-center">
                        <?php the_post_thumbnail([160, 160], [
                            'class' => 'image-responsive'
                        ]); ?>
                    </figure>
                    <table class="col-xs-12">
                        <tr>
                            <th>
                                <div class="pull-left">
                                    <svg>
                                        <use xlink:href="#shape-price"></use>
                                    </svg>
                                </div>
                                <p class="no-margin pull-left">Cena:</p>
                            </th>
                            <td class="text-right">
                                <?php if ((int) $price === -1) { ?>
                                            <p class="no-margin pull-right">Wycena</p>
<?php } else { ?>
<p class="no-margin pull-right"><?php echo $price . ' ' . $currency; ?></p>
<?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <div class="pull-left">
                                    <svg>
                                        <use xlink:href="#shape-time"></use>
                                    </svg>
                                </div>
                                <p class="no-margin pull-left">Czas realizacji:</p>
                            </th>
                            <td class="text-right">
                                <p class="no-margin pull-right"><?php echo $realization; ?></p>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <div class="pull-left">
                                    <svg>
                                        <use xlink:href="#shape-guarantee"></use>
                                    </svg>
                                </div>
                                <p class="no-margin pull-left">Gwarancja:</p>
                            </th>
                            <td class="text-right">
                                <p class="no-margin pull-right"><?php echo $guarantee; ?></p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center" colspan="2">
                                <form method="post" action="">
                                    <input type="submit" name="order" class="button" value="Dodaj do koszyka">
                                </form>
                            </td>
                        </tr>
                    </table>
                </div>
            </aside>

            <aside class="page col-md-6 col-xs-12">
                <div id="content" role="main">

                    <?php while (have_posts()) : the_post(); ?>
                        <?php get_template_part('content', 'page'); ?>
                        <?php comments_template('', true); ?>
                    <?php endwhile; ?>

                </div>
            </aside>

            <aside class="sidebar right col-md-3 col-xs-12">
                <div class="widget pull-left col-xs-12">
                    <h4>Inne usługi</h4>

                    <ul class="another-service list-unstyled">
                        <?php
                        /** @var WP_Post $anotherServicePage */
                        foreach ($anotherServices as $anotherServicePage) {
                            if ($anotherServicePage->ID !== $id) {
                                ?>
                                <li>
                                    <a href="<?php echo get_the_permalink($anotherServicePage->ID); ?>">
                                        <?php echo $anotherServicePage->post_title; ?>
                                    </a>
                                </li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </div>
            </aside>
        </section>
        <?php
    }
get_footer();
?>
